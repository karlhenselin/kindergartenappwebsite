# README #

This is a website.
A Chrome Kiosk App that points to it is in the works.

### What is this repository for? ###

* A website of links appropriate for young English-speaking children.
Attempting to avoid links that have any ads at all, external links, especially to search engines. The goal is that  a Chrome Kiosk App could point to the website and use it like a stand-alone app, but new content isn't necessary, and copyright isn't broken.


### How do I get set up? ###

* Put it on the Internet, or browse the one that is already there, kindergarten.stmls.org

### Contribution guidelines ###

* I welcome requests. You can also email me links you would like added or removed.

### Who do I talk to? ###

* [https://bitbucket.org/account/notifications/send/]KarlHenselin